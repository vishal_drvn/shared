#!/bin/sh
echo "Entering container shell... (you may need to press Enter)"
docker-compose run ${@} --rm --entrypoint bash shared
